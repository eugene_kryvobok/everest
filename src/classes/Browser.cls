public with sharing class Browser {
	private Map<String, Schema.SObjectType> m_objects = Schema.getGlobalDescribe();
	private transient Map<String, Schema.DescribeSObjectResult> m_described_objects = null; //new Map<String, Schema.DescribeSObjectResult>();
	private transient Map<String, Map<String, Schema.DescribeFieldResult>> m_described_fields_by_object = null;
	
	private List<String>                  globalSobjectNames = new List<String>();
	private Map<String, SobjectClass>     Map_globalSobjects = new Map<String, SobjectClass>();
	private Map<String, List<FieldClass>> Map_globalFields   = new Map<String, List<FieldClass>>();


	public String searchStr {get;set;}
	public Boolean showSystemObjBool {get;set;}
	public Boolean showSystemFldBool {get;set;}
	public List<SobjectClass> resultList {get;set;}
	public List<FieldClass> fieldList {get;set;}
	//public List<SobjectClass> allSobjectList {get;set;}
	public String sobjectNameForFields {get;set;}
	
	public Browser() { 
		m_described_objects = new Map<String, Schema.DescribeSObjectResult>();
		List<String> result = new List<String>();
		
		for(String object_name : m_objects.keySet()) { globalSobjectNames.add(object_name); }
		globalSobjectNames.sort();

		for(String object_name : globalSobjectNames) {
			Schema.SObjectType object_type = m_objects.get(object_name);
			Schema.DescribeSObjectResult object_describe_result = object_type.getDescribe();
			SobjectClass sc = new SobjectClass();
			sc.name     = object_describe_result.getName();
			sc.label    = object_describe_result.getLabel();
			sc.isCustom = object_describe_result.isCustom();
			Map_globalSobjects.put(object_name.toLowerCase(), sc);
			m_described_objects.put(object_name.toLowerCase(), object_describe_result);
		}
		searchStr = '';
		showSystemObjBool = false;
		getResults();
	}

	public Pagereference getResults() {
		fieldList = new List<FieldClass>();
		resultList = new List<SobjectClass>();
		SobjectClass curSobject;
		for(String item : globalSobjectNames) {
			curSobject = Map_globalSobjects.get(item.toLowerCase());
			if (showSystemObjBool == false && curSobject.isCustom == false) {}
			else {
				if (curSobject.name.toLowerCase().contains(searchStr.toLowerCase()) || curSobject.label.toLowerCase().contains(searchStr.toLowerCase())) {
					curSobject.isLoad = false;
					resultList.add(curSobject);
				}
			}
		}
		return null;
	}
	//sobjectNameForFields
	public Pagereference getFields() {
		Map_globalSobjects.get(sobjectNameForFields.toLowerCase()).fields = getFieldList(m_described_objects, sobjectNameForFields.toLowerCase());
		Map_globalSobjects.get(sobjectNameForFields.toLowerCase()).isLoad = true;
		fieldList = getFieldList(m_described_objects, sobjectNameForFields.toLowerCase()); //Map_globalSobjects.get(sobjectNameForFields.toLowerCase()).fields;
		
		return null;
	}
	
	
	private List<FieldClass> getFieldList(Map<String, Schema.DescribeSObjectResult> m_described_objects, String p_object_name) {
		m_objects = Schema.getGlobalDescribe();
		Schema.SObjectType object_type = m_objects.get(p_object_name);
		Schema.DescribeSObjectResult object_describe_result = object_type.getDescribe();
		
		//RefreshObjects();
		List<FieldClass> result = new List<FieldClass>();
		system.debug('m_described_objects===' + m_described_objects);
	    //Schema.DescribeSObjectResult object_describe_result = m_described_objects.get(p_object_name.toLowerCase());
	    Map<String, Schema.SObjectField> object_fields = object_describe_result.fields.getMap();
	    
	    List<String> fieldNamesSort = new List<String>();
	    fieldNamesSort.addAll(object_fields.keySet());
	    fieldNamesSort.sort();
	    
		for(String field_name : fieldNamesSort) {
			Schema.DescribeFieldResult field_describe_result = object_fields.get(field_name.toLowerCase()).getDescribe();
			FieldClass fc = new FieldClass();
			fc.name = field_describe_result.getName();
			fc.label = field_describe_result.getLabel();
			fc.isCustom = field_describe_result.isCustom();
			if (showSystemFldBool == false && fc.isCustom == false) {}
			//Schema.SObjectField 
			else 
				system.debug('fieldName===' + field_name);
	 			result.add(fc);
		}
		system.debug('getFieldList===' + result);
		return result;
	}

	public class SobjectClass {
		public String name {get;set;}
		public String label {get;set;}
		public Boolean isCustom {get;set;}
		public List<FieldClass> fields {get;set;}
		public Boolean isLoad {get;set;}
		public SobjectClass() {
			isLoad = false;
		}
	}

	public class FieldClass {
		public String name {get;set;}
		public String label {get;set;}
		public Boolean isCustom {get;set;}
	}
	
	public static testMethod void testThis() {
		Browser con = new Browser();
		con.getResults();
		con.sobjectNameForFields = 'account';
		con.getFields();
	}

}