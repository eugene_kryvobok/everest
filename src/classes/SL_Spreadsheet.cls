public with sharing class SL_Spreadsheet {
    public String accountName { get; set; }
    public static Account account { get; set; }
    public static Account getAccount(String accountName) {
        account = [select id, name, phone, type, numberofemployees from 
             Account where name = :accountName ];
        return account;
    }
	
	public SL_Spreadsheet() {
//		List<String> sss = new List<String>(');
	}
	
	public List<String> getHeaderLabels() {
		List<String> result = new List<String>();
		for(integer i=0; i < 10; i++) {
			result.add(''+i);
		}
		return result;
	}
	
	public List<RowClass> getTableRows() {
		List<RowClass> result = new List<RowClass>();
		for(integer i=0; i < 10; i++) {
			RowClass newRC = new RowClass();
			for(integer j=0; j < 10; j++) {
				newRC.cells.add(''+ (i * 10 + j));
			}
			result.add(newRC);
		}
		return result;
	}
	
	public class RowClass {
		public List<String> cells {get;set;}
		public RowClass() {
			cells = new List<String>(); 
		}
	}
}