/**
* @ClassName 	 : SL_LIB36_Fixedheader_Controller
* @JIRATicket    : LIB-36
* @CreatedOn     : 5/Feb/13
* @ModifiedBy    : Hemant
* @Description   : This class is for preparing records to display in a table with fixed header.
*/

public with sharing class SL_LIB36_Fixedheader_Controller
{
	/* Start - Variables */
    public List<String> lstSobjectHeader			{	get;set;	}
    public List<String> lstSobjectFields			{	get;set;	}
    public List<FieldWrapper> lstFieldWrapper		{	get;set;	}
    public boolean isPrevious						{	get;set;	}
	public boolean isNext 							{	get;set;	}
	public Integer pageNumber						{	get;set;	}
	public SL_LIB9_Paginator objPaginator			{	get;set;	}
	public Integer totalPages						{	get;set;	}
	public string sortExpression					{	get;set;	}
	public string sortDirection						{	get;set;	}
    
    public static final string SOBJECT_NAME = 'Account'; //SObject Name 
	public static string FIELDS_DEF = 'Name,AccountNumber,Fax,Industry,Phone'; //Comma Seprated Field Name;May also contain fieldSetName of the sObject
	public static final Integer PAGE_SIZE = 200; //Record per page
	public static  string WHERE_CONDITION = ''; // Where condition of the SOQL
	
	/* End - Variables */
	/* Start - Inner class for fields api and label wrapper */
	public class FieldWrapper
	{
		public String fieldAPI		{	get;set;	}
		public String fieldLabel	{	get;set;	}
		public FieldWrapper(String fieldAPI, String fieldLabel)
		{
			this.fieldAPI = fieldAPI;
			this.fieldLabel = fieldLabel;
		}
	}
	/* End */
    
	/* Start - Constructor */
    public SL_LIB36_Fixedheader_Controller()
    {
    	initialize();
    	
    	//getFieldsFromFieldSet();
    	/* Preparing a list of API Names of fields to be displayed*/        
        lstSobjectFields = new list<String>{'Name','AccountNumber','Fax','Industry','Phone'};
        
        /* One more list needs to be prepared for Column Lables */
        lstSobjectHeader = new list<String>{'Name','AccountNumber','Fax','Industry','Phone'};
        
		objPaginator = new SL_LIB9_Paginator(FIELDS_DEF , PAGE_SIZE , SOBJECT_NAME , WHERE_CONDITION , false );
		if(objPaginator.isValidParameters)
		{
			objPaginator.setSortingQueryString('Name', 'ASC');
			objPaginator.gotoFirstPage();
			setDataMembers();
		}
    }
    /* End - Constructor */
    
    /*
	
    Method name	: getFieldsFromFieldSet
    Params		: NA
    Description	: get the fields from the filed set .
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    private void getFieldsFromFieldSet()
    {
    	Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
    	Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(SOBJECT_NAME);
	    Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
	    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('AccountFieldSet');
	    for(Schema.FieldSetMember fieldSet : fieldSetObj.getFields())
	    {
	    	FIELDS_DEF += fieldSet.getFieldPath() + ',';
	    	lstFieldWrapper.add(new FieldWrapper(fieldSet.getLabel(),fieldSet.getFieldPath()));
	    }  
	   	
	   	FIELDS_DEF = FIELDS_DEF.substring(0,FIELDS_DEF.length()-1);
    }
    
    /*
	
    Method name	: initialize
    Params		: NA
    Description	: To intialize all the public and private variables of the controller.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    private void initialize()
    {
    	isPrevious = false;
	 	isNext = false ;
	 	lstFieldWrapper = new List<FieldWrapper>();
    }
    /* End */
    
    /*
	
    Method name	: goToFirstPage
    Params		: NA
    Description	: To get the records of the first page.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    public void goToFirstPage()
	{
		objPaginator.gotoFirstPage();
		setDataMembers();	
	}
    /* End */
    
    /*
	
    Method name	: goToLastPage
    Params		: NA
    Description	: To get the records of the last page.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    public void goToLastPage()
	{
		objPaginator.gotoLastPage();
		setDataMembers();
	}
	/* End */
	
	/*
	
    Method name	: goToSpecificPage
    Params		: NA
    Description	: To get the records of the specific page.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    public void goToSpecificPage()
	{
		integer specificPageNumber =null;
		if(ApexPages.currentPage().getParameters().get('pageNumber') != null)
			specificPageNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get('pageNumber'));
		if(specificPageNumber != null)
		{
			objPaginator.goToSpecificPage(specificPageNumber); 
			setDataMembers();
		}
	}
	/* End */
	
	/*
	
    Method name	: goToNextPage
    Params		: NA
    Description	: To get the records of the next page.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
    public void goToNextPage()
	{
		objPaginator.gotoNextPage();
		setDataMembers();
	}
	/* End */
	
	/*
	
    Method name	: goToPreviousPage
    Params		: NA
    Description	: To get the records of the previous page.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
	public void goToPreviousPage()
	{
		objPaginator.gotoPrevPage();
		setDataMembers();
	}
	/* End */
	
	/*
	
    Method name	: getSortRecord
    Params		: NA
    Description	: To get the sorted records.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
	public void getSortRecord()
	{
		objPaginator.getSortingRecords(sortExpression, pageNumber);
		setDataMembers();
	}
	/* End */
	
	/*
	
    Method name	: setDataMembers
    Params		: NA
    Description	: To set the data memebers.
    Date		: 22/Apr/2013
    Modified by	: Vishnu
    */
	private void setDataMembers()
	{
		sortExpression = objPaginator.strOrderByFieldName;
		sortDirection =  objPaginator.strOrderByDirection;
		pageNumber = objPaginator.intPageNumber;
		isPrevious = objPaginator.isPreviousDisabled ;
		isNext = objPaginator.isNextDisabled;
		totalPages = objPaginator.intTotalPages;
	}


    static Account objTestAccount1;
	static Account objTestAccount2;
	static Account objTestAccount3;

	// Create records for Test Coverage
	private static void createTestRecords()
	{
		objTestAccount1 = new Account(Name = 'Test Account 1');
		objTestAccount2 = new Account(Name = 'Test Account 2');
		objTestAccount3 = new Account(Name = 'Test Account 3');
		List<Account> lstTestAccount = new List<Account>{objTestAccount1,objTestAccount2,objTestAccount3};
		for(Integer i=1; i<=400; i++)
			lstTestAccount.add(new Account(Name='Test Account-'+i));
		insert lstTestAccount;
	}

	
	private static testMethod void SL_LIB36_Fixedheader_Controller_goToFirstPage ()
	{
        createTestRecords() ;
	    SL_LIB36_Fixedheader_Controller objFixedHeaderController = new SL_LIB36_Fixedheader_Controller(); 
	    objFixedHeaderController . getFieldsFromFieldSet();
	    objFixedHeaderController .pageNumber = 3 ;
		objFixedHeaderController . goToFirstPage();
	}
	
	
	private static testMethod void SL_LIB36_Fixedheader_Controller_goToLastPage ()
	{
        createTestRecords() ;
		
	    SL_LIB36_Fixedheader_Controller objFixedHeaderController = new SL_LIB36_Fixedheader_Controller(); 
	    objFixedHeaderController . getFieldsFromFieldSet();
	    objFixedHeaderController .pageNumber = 5 ;
		objFixedHeaderController . goToLastPage();
	}
	
	private static testMethod void SL_LIB36_Fixedheader_Controller_goToSpecificPage()
	{
        createTestRecords() ;
	    SL_LIB36_Fixedheader_Controller objFixedHeaderController = new SL_LIB36_Fixedheader_Controller(); 
	    objFixedHeaderController . getFieldsFromFieldSet();
	    objFixedHeaderController .pageNumber = 1 ;
	    ApexPages.currentPage().getParameters().put('PageNumber','1') ;
		objFixedHeaderController . goToSpecificPage();
		system.assert(objFixedHeaderController.pageNumber == 1);
		
	}
	
	private static testMethod void SL_LIB36_Fixedheader_Controller_gotoNextPage ()
	{
        createTestRecords() ;
		
	    SL_LIB36_Fixedheader_Controller objFixedHeaderController = new SL_LIB36_Fixedheader_Controller(); 
	    objFixedHeaderController.pageNumber = 1 ;
	    objFixedHeaderController.goToNextPage() ;
	    system.assert(objFixedHeaderController.pageNumber == 2);
	}
	
	
	private static testMethod void SL_LIB36_Fixedheader_Controller_gotoPreviousPage ()
	{
        createTestRecords() ;
		
	    SL_LIB36_Fixedheader_Controller objFixedHeaderController = new SL_LIB36_Fixedheader_Controller(); 
	    objFixedHeaderController.pageNumber = 2 ;
		objFixedHeaderController.goToPreviousPage();
		objFixedHeaderController.getSortRecord();
	}
}