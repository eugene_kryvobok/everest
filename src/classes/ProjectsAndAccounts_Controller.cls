public class ProjectsAndAccounts_Controller {
	
	public List<MyProject> getProjectsAndAccounts() {
		List<MyProject> resultList = new List<MyProject>();
		List<Account__c> accountList = [
			SELECT Name, Sandbox_Production__c, Login__c, Password__c, Token__c, Project__c 
			FROM Account__c
			ORDER BY Name];
			
		for (Project__c projItem : [SELECT Id, Name FROM Project__c ORDER BY Name]) {
			MyProject newProject = new MyProject();
			newProject.Id = projItem.Id;
			newProject.projectName = projItem.Name;
			newProject.accountList = new List<MyAccount>();
			
			for(Account__c accItem : accountList) {
				if (accItem.Project__c == projItem.Id) {
					MyAccount newAccount 			= new MyAccount();
					newAccount.accountName 			= accItem.Name;
					newAccount.sandbox_production 	= accItem.Sandbox_Production__c;
					newAccount.login 				= accItem.Login__c;
					newAccount.password 			= accItem.Password__c;
					newAccount.token 				= accItem.Token__c;
					newProject.accountList.add(newAccount);
				}
			}
			
			resultList.add(newProject); 
		}
		return resultList; 
	}
	public class MyProject {
		public String Id {get;set;}
		public String projectName {get;set;}
		public List<MyAccount> accountList {get;set;}
	}
	
	public class MyAccount {
		public String accountName {get;set;}
		public String sandbox_production {get;set;}
		public String login {get;set;}
		public String password {get;set;}
		public String token {get;set;}
	}
	public static testMethod void testThis() {
		ProjectsAndAccounts_Controller con = new ProjectsAndAccounts_Controller();
		con.getProjectsAndAccounts();
	}
}