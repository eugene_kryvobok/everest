public with sharing class Test_Obj1 {
	public static testMethod void testThis() {
		Obj1__c testObj1 = new Obj1__c(name='123');
		insert testObj1;
		testObj1.name = '234';
		update testObj1;
	}
}