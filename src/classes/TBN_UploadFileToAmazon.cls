public without sharing class TBN_UploadFileToAmazon 
{
	/** Start - All variables */
	public String strFileName {get;set;}	
	public String strBucketName {get;set;}		
	public AWSKeys credentials {get;set;}	
	public S3.AmazonS3 as3 { get; private set; }	
	private String AWSCredentialName = 'Amazon Credential';	  
	public String secret { get {return credentials.secret;} }	
	public String key { get {return credentials.key;} }	
	public String strfileContentType	{get;set;}	
	public String strformattedexpire	{get;set;}	
	public String strCaseAttachmentId{get;set;}	
	public String strBaseUrl {get;set;}	
	public String strfileSize {get;set;} 
	
	/** End - All variables */
	
	/* Start : variables used for signing the file being uploaded */
	Datetime expire = system.now().addDays(1);
	String formattedexpire = strformattedexpire = expire.formatGmt('yyyy-MM-dd') + 'T' +   
	expire.formatGmt('HH:mm:ss') + '.' + expire.formatGMT('SSS') + 'Z';           
	      
	String policy { get {return 
		'{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+  
    	strBucketName +'" } ,{ "acl": "private" },'+
    	'{"content-type":"'+strfileContentType+'"},'+
    	'{"success_action_redirect": "'+strBaseUrl+'/'+strCaseAttachmentId+'"}, ' +
    	'["starts-with", "$key", "' + strFileName + '"] ]}'; 	}
    	 } 
    	 
    /* End : variables used for signing the file being uploaded */
    
    /**
	* Constructor: Fetches the case id from url and initialize all the global variables.
	* @param : Apexpages.StandardSetcontroller of Amazon_Attachment__c.
	*/
    public TBN_UploadFileToAmazon()
	{		
		try
		{
			Initialize(); //  Initialize all the global variables
			datetime expireDate = system.now().addDays(1);
			strformattedexpire = expireDate.formatGmt('yyyy-MM-dd') + 'T' +
			expireDate.formatGmt('HH:mm:ss') + '.' + expireDate.formatGMT('SSS') + 'Z';  
			
			// fetch the base URL of current org.
			String strUrl = string.valueOf(url.getSalesforceBaseUrl());
			if(strUrl.contains('=') && strUrl.contains(']'))
	 			strBaseUrl = strUrl.subString(strUrl.indexOf('=')+1, strUrl.indexOf(']'));
	 		else
	 			strBaseUrl = strUrl;
	 		actionOnLoad();	
		}
		catch(Exception e)
		{
			system.debug('-Exception-' + e);
		}		
	}
	
	/*
		@MethodName: Initialize 
		@param None
		@Description: Initialize all the global variables
	*/
	public void Initialize() 
	{
    	strFileName = strfileSize = '';
		strBucketName = '';
		strfileContentType = '';
		strBaseUrl = '';
	}
	
	/*
		@MethodName: getPolicy 
		@param None
		@Description: create and encode the policy.
	*/
	public String getPolicy() 
	{
    	return EncodingUtil.base64Encode(Blob.valueOf(policy));
	}  
	
	/*
		@MethodName: OnLoadAction 
		@param None
		@Description: fetch the key and secret ket of amazon org and fetch the bucket name from custom setting.
	*/
	public void actionOnLoad()
	{
		try
		{
			//fetching Amazon org secret key and key  
			credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);		
			//fetching bucket name from custom setting	
			AmazonBucket__c b = AmazonBucket__c.getOrgDefaults();	   
			if(b != null) {
				strBucketName = b.Value__c;	
				system.debug('strBucketName===' + strBucketName);
			} else {
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon bucket name');
				return;
			}
		}
		catch(AWSKeys.AWSKeysException AWSEx)
		{
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);			
		}	
		return;	
	}
	
	/*
		@MethodName: getSignedPolicy 
		@param None
		@Description: form the signature for form post.
	*/	   
    public String getSignedPolicy() 
    {   
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    } 
    
    /*
		@MethodName: make_sig 
		@param None
		@Description: return the mac url for the signature created.
	*/
    private String make_sig(string canonicalBuffer)      
    { 
    	try
    	{       
	        String macUrl ;
	        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
	        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
	        macUrl = EncodingUtil.base64Encode(mac); 	              
	        return macUrl;
    	}
    	catch(Exception e)
    	{
    		system.debug('-Exception-' + e);
    		return null;
    		//Do Nothing.
    	}
    }
    
	/*
		@MethodName: fetchRelatedContentType 
		@param None
		@Description: dummy function called on selection of file and rerender the form post.
	*/
	public void dummyFunction() 
	{
		//Do nothing
    }
    
    /*
		@MethodName: submitCaseAttachment 
		@param None
		@Description: function called on click of 'No, Thanks. Submit Case'. It inserts Case Attachment record
	*/
    public void submitCaseAttachment()
    {
    	try
    	{
			Amazon_Attachment__c objCaseAttach = new Amazon_Attachment__c();
			objCaseAttach.File_Type__c = strfileContentType;
			objCaseAttach.File_Name__c = strFileName;
			objCaseAttach.Name = strFileName;
			objCaseAttach.Link_To_Download__c = strBaseUrl + '/apex/TBN_DisplayAmazonFile?BucName=' + strBucketName + '&FileName=' + strFileName;
			objCaseAttach.File_Size__c = strfileSize + 'kb';	
			
			if(objCaseAttach != null)
				insert objCaseAttach;
				 
			strCaseAttachmentId = objCaseAttach.Id;		
			strFileName = objCaseAttach.Id + '_' + strFileName ;
			
			objCaseAttach.Link_To_Download__c = strBaseUrl + '/apex/TBN_DisplayAmazonFile?BucName=' + strBucketName + '&FileName=' + strFileName;
			update objCaseAttach;
    	}
    	catch(Exception e)
    	{
			system.debug('-Exception-' + e);   		
    	}	
    }
}