public with sharing class InputFileAttachment {
	public String filename {get; set;}
    public blob Attach {get;set;}  
    string CurrentId ;
   
    public InputFileAttachment ()
    {      
        CurrentId = ApexPages.CurrentPage().getParameters().get('Id');      
    }   
    public pagereference AttachNewFile()
    {
        try
        {
        	delete [select id from Attachment where ParentId=:CurrentId];
        	Blob b = Attach;
        	Attachment At = new Attachment(
        		//Name ='NewFile'+'.jpg',
        		Name=filename,
        		body = b,parentId=CurrentId
        	);
        	insert At;       
        }Catch(Exception ee){}
        return null;
    }
}