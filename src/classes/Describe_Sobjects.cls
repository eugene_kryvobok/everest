public class Describe_Sobjects {  
	
	public List<RowClass> getRowList() {
		Map<String, Schema.SObjectType> map_objName_object = Schema.getGlobalDescribe();
		List<String> objectNameList = new List<String>();
		objectNameList.addAll(map_objName_object.keySet());
		objectNameList.sort();
		
		List<RowClass> resultList = new List<RowClass>();
		Schema.DescribeSObjectResult curDSOResult;
		for(String item : objectNameList) {
			curDSOResult = map_objName_object.get(item).getDescribe();
			RowClass newRow = new RowClass();
			
			newRow.itemList.add(new ItemClass('activateable',  ''+curDSOResult.isAccessible()));
			newRow.itemList.add(new ItemClass('createable',    ''+curDSOResult.isCreateable()));
			newRow.itemList.add(new ItemClass('custom',        ''+curDSOResult.isCustom()));
			newRow.itemList.add(new ItemClass('customSetting', ''+false));
			newRow.itemList.add(new ItemClass('deletable',     ''+curDSOResult.isDeletable()));
			newRow.itemList.add(new ItemClass('feedEnabled',   ''+false));
			newRow.itemList.add(new ItemClass('keyPrefix',     ''+curDSOResult.getKeyPrefix()));
			newRow.itemList.add(new ItemClass('label',         ''+curDSOResult.getLabel()));
			newRow.itemList.add(new ItemClass('labelPlural',   ''+curDSOResult.getLabelPlural()));
			newRow.itemList.add(new ItemClass('layoutable',    ''+false));
			newRow.itemList.add(new ItemClass('mergeable',     ''+curDSOResult.isMergeable()));
			newRow.itemList.add(new ItemClass('name',          ''+curDSOResult.getName()));
			newRow.itemList.add(new ItemClass('queryable',     ''+curDSOResult.isQueryable()));
			newRow.itemList.add(new ItemClass('replicateable', ''+false));
			newRow.itemList.add(new ItemClass('retrieveable',  ''+false));
			newRow.itemList.add(new ItemClass('searchable',    ''+curDSOResult.isSearchable()));
			newRow.itemList.add(new ItemClass('triggerable',   ''+false));
			newRow.itemList.add(new ItemClass('undeletable',   ''+curDSOResult.isUndeletable()));
			newRow.itemList.add(new ItemClass('updateable',    ''+curDSOResult.isUpdateable()));
			newRow.itemList.add(new ItemClass('urlDetail',     ''));
			newRow.itemList.add(new ItemClass('urlEdit',       ''));
			newRow.itemList.add(new ItemClass('urlNew',        ''));
			
			resultList.add(newRow);
		}
		
		return resultList;
	}
	
	
	
	public class RowClass {
		public List<ItemClass> itemList {get;set;}
		
		public RowClass() {
			itemList = new List<ItemClass>();
		}
	}
	
	public class ItemClass {
		public String tagName {get;set;}
		public String value   {get;set;}
		
		public ItemClass(String p_tagName, String p_value) {
			tagName = p_tagName;
			value   = p_value;
		}
	}
	
	public static testMethod void testThis() {
		Describe_Sobjects con = new Describe_Sobjects();
		con.getRowList();
	}
	
}