/**
* @ClassName    : SL_LIB89_CustomErrorPreview 
* @JIRATicket   : LIB-89
* @CreatedOn    : 7/May/2013
* @ModifiedBy   : SL
* @Description  : Controller for the page SL_LIB89_CustomErrorPreview
*/

/**
@Developer Name       : Harsh
Percentage of best practices followed : 100%
No of SOQL queries used     : 0
No of collections used     : 0
Exception Handling implemented   : No
Coding standards followed    : Yes
Naming conventions followed    : Yes
Third party integrations    : No
Maximum of No of records tested with : 
Customer Approval      : 
Last Modified Date      : 
Approved by        : 

*/

public with sharing class SL_LIB89_CustomErrorPreview 
{
	public String strSeverity		{get;set;}   // To store the value of the severity
	public String strCustomMessage	{get;set;}   // To store the text value of the message to be displayed on the component.
	public Boolean isDisplayMessage	{get;set;}   // Boolean to conditionally render the custom message component.
	public String strBgColor        {get;set;}	 // To store the value of background color for the component
	public String strfontsize       {get;set;}	 // To store the value of fontsize for the Component content
	
	public SL_LIB89_CustomErrorPreview()
	{
		// Initialize the global variables
		strSeverity = strCustomMessage ='';
		strBgColor = 'yellow';
		isDisplayMessage = false;
		strfontsize = '30px';
	}
	
	
	/*
		@MethodName     : showMessage 
        @params         : none
        @Description    : method called on click of 'Show Message' button on page
     */
	public void showMessage()
	{
		//Boolean value isDisplayMessage is made true
		isDisplayMessage = true;
	}
	/* End of Method */
	
	/* Start - Test Method*/
	@isTest
	private static void test_SL_LIB89_CustomErrorPreview()
	{
		SL_LIB89_CustomErrorPreview objCEP = new SL_LIB89_CustomErrorPreview(); 
		objCEP.showMessage();
	}
	/* End */
}