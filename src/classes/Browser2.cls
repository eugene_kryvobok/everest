global class Browser2 {

    @RemoteAction
    global static String getAccount() {
        //List<Account__c> account = [select id, name from Account__c];
        String account = '[';
        account += '{id: "123", text:"First Level Child 1"}';
        account += ',{id: "2", text:"First Level Child 2",children:[{text:"Second Level Child 1",leaf:true},{text:"Second Level Child 2",leaf:true}]}';
        account += ',{id: "3", text:"First Level Child 3",children:[{text:"Second Level Child 1",leaf:true},{text:"Second Level Child 2",leaf:true}]}';
        account += ',{id: "4", text:"First Level Child 4",children:[{text:"Second Level Child 1",leaf:true},{text:"Second Level Child 2",leaf:true}]}';
        account += ',{id: "5", text:"First Level Child 5",children:[{text:"Second Level Child 1",leaf:true},{text:"Second Level Child 2",leaf:true}]}';
        account += ',{id: "6", text:"First Level Child 6",children:[{text:"Second Level Child 1",leaf:true, icon: "/resource/1309560752000/Extjs3/resources/images/default/tree/folder.gif"},{text:"Second Level Child 2",leaf:true}]}';
        account += ']';
        return account;
    }  
}